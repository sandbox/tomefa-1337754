// $Id: README.txt,v 1.0 2011/11/10 15:50:00 Tomefa Exp $

Action Email all author
------------------------
This module create a new Drupal action name "Action Email all author" that can be use on the trigger page.
It will send a mail to all the people involved in the node creation :
- author of the node
- user that create revision of the node
- user that made change on the workflow of the node

You can custom the module by changing the email subject and body
And also disabled the workflow request or the revision request

Dependencies :
- Workflow module

Installation
-----------
To install, place the entire Action Email all author folder into your modules directory.
Go to Administer -> Site building -> Modules and enable the Action Email all author module

Maintainers
-----------
The Action Email all author was originally developped by:
Tom Blanchard
